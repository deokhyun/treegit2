#ifndef __GLOBALS_H
#define __GLOBALS_H

//typedef unsigned char BOOLEAN; /* Logical data type (TRUE or FALSE) */
//typedef unsigned char INT8U; /* Unsigned 8 bit value */
//typedef signed char INT8S; /* Signed 8 bit value */
//typedef unsigned short INT16U; /* Unsigned 16 bit value */
//typedef signed short INT16S; /* Signed 16 bit value */
//typedef unsigned long INT32U; /* Unsigned 32 bit value */
//typedef signed long INT32S; /* Signed 32 bit value */
//typedef float FP32; /* 32 bit, single prec. floating-point */
//typedef double FP64; /* 64 bit, double prec. floating-point */
//typedef double INT64S;


typedef unsigned char BOOLEAN; /* Logical data type (TRUE or FALSE) */
typedef unsigned char INT8U; /* Unsigned 8 bit value */
typedef signed char INT8S; /* Signed 8 bit value */
typedef unsigned short INT16U; /* Unsigned 16 bit value */
typedef signed short INT16S; /* Signed 16 bit value */
typedef unsigned int INT32U; /* Unsigned 32 bit value */
typedef signed int INT32S; /* Signed 32 bit value */
typedef float FP32; /* 32 bit, single prec. floating-point */
typedef double FP64; /* 64 bit, double prec. floating-point */
typedef double INT64S;


#define _PACKED_DEF __packed
#define ROMDATA	const
#define HUGEDATA
#define ON  1
#define OFF 0

#define TRUE  1
#define FALSE 0

#define AD_DRV_AD7177 //32bit ADC
//#define AD_DRV_AD7714 //24bit ADC


#endif /* __GLOBALS_H */