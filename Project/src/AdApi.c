#include "globals.h"
#include "AdApi.h"
#include "Adm_def.h"
#include "Adm_var.h"
#include "AdDrv.h"
#include "RingBuf.h"


long RawAd;

long AdBuf[NUM_ADBUFFER];

void AdcInit(void)
{

	AdDrvInit();
    TempAdDrvInit();
}
INT32U Ad_Filter (INT32U rawData);

INT32U Get_ad (void)
{

    INT32U rawData;
    rawData = 0;
    AdDrvProc(); //kevin : 최종 변환된 AD 32비트 AD 신호가 &AdDrvRingBuf 주소값에 넣어진다. 참고로 이니셜 할 때 adRingBufPtr = &AdDrvRingBuf; 였음
    
    if(RingBufCheckBuf(adRingBufPtr)) //kevin : 따라서 32비트 신호가 나오기 시작하면 adRingBufPtr 값이 무언가가 있기에 여기로 진입한다.
    {
        rawData = RingBufGetInt32u(adRingBufPtr); //kevin : 위에까지 컨버팅되고, 버퍼에 저장되어있는 데이터를 rawData 에 넣어준다.
        //GPIO_ToggleBits(GPIOA,GPIO_Pin_2); // kevin : 하드웨어상 LED 와 연결되어있다. (컨버젼할때마다 LED를 깜빡이고 싶었나보다.
        //rawData = Ad_Filter(rawData);
    }
    return rawData; //kevin : 저장된 rawData 를 리턴
}

INT32U Get_TempAd (void)
{

    INT32U rawData;
    rawData = 0;
    TempAdDrvProc();
    
    if(RingBufCheckBuf(TempAdRingBufPtr))
    {
        rawData = RingBufGetInt32u(TempAdRingBufPtr);
    }
    
    return rawData;

}



#define MAFDevideNum 5
#define MAFSize 32 //2의 MAFDevideNum 승 한 값 ex) MAFDevideNum 가 4면 2의4승 = 16

INT32U Ad_Filter (INT32U rawData) //kevin : 20170626 새로만든 함수
{
    static INT32U MAFBuff[MAFSize];
    static INT16U zeroDelayCnt = 1;
    static INT32U rawAdZeroData = 0;
    INT32U rawAdData;
    long long sum;
    INT8U i ;
    
    rawData = rawData;
    
    for(i = 0; i < MAFSize - 1; i++)
    {
        MAFBuff[i] = MAFBuff[i + 1];
    }

    MAFBuff[MAFSize - 1] = rawData;
    
    sum = 0;
    
    for(i = 0; i < MAFSize; i++)
    {
        sum += MAFBuff[i];
    }

    rawAdData = sum >> (MAFDevideNum);

    if (zeroDelayCnt  !=  0)
    {
        zeroDelayCnt++;
    }
    
    if (zeroDelayCnt > (8 * 5))
    {
        //rawAdZeroData = rawAdData - 1000;
        zeroDelayCnt = 0;
    }
    rawAdData = rawAdData - rawAdZeroData;
    
    return rawAdData;
}    
        

