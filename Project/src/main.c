/**
  ******************************************************************************
  * @file    Project/STM32F4xx_StdPeriph_Templates/main.c 
  * @author  MCD Application Team
  * @version V1.7.1
  * @date    20-May-2016
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2016 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "globals.h"
#include "arm_math.h"
#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "stm32f4xx_rtc.h"
#include "main.h"
#include "lcd.h"
#include "RingBuf.h"
#include "commbuf.h"
#include "comm_app.h"
#include "timer.h"
#include "eeprom.h"
#include "buzzer.h"
#include "key.h"
#include "AdApi.h"
#include "filter.h"
/** @addtogroup Template_Project
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* FFT settings */
/* Private variables ---------------------------------------------------------*/
static __IO uint32_t uwTimingDelay;
RCC_ClocksTypeDef RCC_Clocks;
RTC_InitTypeDef RTC_InitStructure;
__IO uint32_t   uwLsiFreq = 0;
__IO uint32_t   uwCaptureNumber = 0; 
__IO uint32_t   uwPeriodValue = 0;
/* Private function prototypes -----------------------------------------------*/
    void RCC_Init(void);
static void Delay(__IO uint32_t nTime);
static void RTC_Config(void);
void SPI_Config(void);
void OutString(char *s);
void rcc_clock_init(void);
void gpio_init(void);
void nvic_init(void);
void usart1_init(void);
void timer2_init(void);
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
INT8U msgBuf[200];
INT8U adReadFlag = 0x00;
INT8U TempAdReadFlag = 0x00;
char rawAdDataASCII[11];   //kevin_c : Int 형에서 ASCII 로 데이터변환을 위한 변수 

extern INT8U Int32sToDecStr(char *dst, INT32U numData, INT8U size, char fillChar, INT8U decPos, char decChar, INT8U useThousandPt);
static INT32U rawAdDatatest = 0;
extern void SendTempRawDATA(char *data);

// git branch test

int main(void)
{
    INT8U state = 0;
    INT8U revChar = 0;
    INT8U eepTest = 0x33;
    INT8U eepData = 0x00;
    INT8U key = 0x00;
    INT8U dispMsg[200];
    INT32U rawAdData = 0;
    INT32U rawTempAdData = 0;
    INT32U FlAdData;
    INT32U lcdTestCnt = 0;
    INT32U dispRawWad = 0;
    INT32U dispRawTad = 0;
    static INT32U tempdatatemp = 0;
        
    /* SysTick end of count event each 10ms */
    SystemCoreClockUpdate();
    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);
    
    /* Enable Clock Security System(CSS): this will generate an NMI exception
    when HSE clock fails *****************************************************/
    RCC_ClockSecuritySystemCmd(ENABLE);
    RCC_GetClocksFreq(&RCC_Clocks);

    
    rcc_clock_init();
    gpio_init();

    timer2_init();
    Key_Init();
    Commbuf_Init();
    usart1_init();
    SPI_Config();
    init_lcd();
	lcd_set();
	Dsp_Clear();
    TimerInit();
    buzzer_init();
    nvic_init();
    AdcInit(); // duckspg 

    WC300_MAF_Init(&Maf1stOrder,MAF_1ST_ORDER_WIN_SIZE,MAF_1ST_ORDER);
    TimerSetDelay100ms(TIMER_DELAY_100MS_SEND_DATA,1);
     
    GPIO_ResetBits(GPIOB,GPIO_Pin_0);
     GPIO_SetBits(GPIOC,GPIO_Pin_5);
    
    while (1)
    {

        rawAdData = Get_ad(); //kevin : 32비트 컨버전된 AD 데이터를 버퍼에 쓴다음 다시 읽어서 그 값을 rawAdData에 넣는 작업.
        rawTempAdData = Get_TempAd(); //kevin : 온도 AD값

        if(rawTempAdData)
        {
            tempdatatemp = rawTempAdData;
        }
        
        
        if(rawAdData)
        {
            rawAdDatatest = rawAdData;
            Int32sToDecStr(rawAdDataASCII,rawAdData,10,0,0,0,0);        
            SendAdRawDATA(rawAdDataASCII); //kevin : adData 를 UART 로 전송
            
            rawAdDatatest = tempdatatemp;
            Int32sToDecStr(rawAdDataASCII,rawAdDatatest,10,0,0,0,0);        
            SendTempRawDATA(rawAdDataASCII);
            
        }
        //kevin? : 왜 raw데이터는 이미 32비트 값을 가지고 있을까, 자동 형변환 떄문에?
        
        //if(KeyCheck())
        if(0)
        {
           key = RingBufGetChar(&KeyDrvRingBuf);
           Buzzer_On(1);
           PutCharTxBuf(&CommBufUart1,key+0x30); //아스키값으로 변환하기위해 0x30 더함 (0x30=0 0x31=1 0x32=2....) 그리고 이걸 uart 로 던져줌

           switch(key)
           {
               case 0x01:
                   Eeprom_bwrite(0x00000000,(INT8U*)&eepTest);
                   memset(dispMsg,0x00,sizeof(dispMsg));
                   sprintf(dispMsg,"EEPROM WRITE TEST [0x%2X]",eepTest);
                   break;
                   
               case 0x02:
                   eepData = Eeprom_bread(0x00000000);
                   memset(dispMsg,0x00,sizeof(dispMsg));
                   sprintf(dispMsg,"EEPROM READ TEST [0x%2X]",eepData);
                   if(eepData == eepTest) Buzzer_On(2);
                   break;
                   
               case 0x03:
                   Dsp_Clear();
                   LCDAddr = 0;
                   LcdPtr = 0;
                   break;
               case 0x04:
                   break;
               case 0x05:
                   TempAdReadFlag = 1;
                   break;
           }
        }
        
        
        //if (CheckRxBuf(&CommBufUart1)) 
        if (0) 
        {
            revChar = GetCharRxBuf(&CommBufUart1);
        
            switch(state)
            {
                case 0:
                    if(revChar=='c') state = 1;
                    else if(revChar=='e') state = 4;
                    else if(revChar=='b') state = 7;
                    break;
                case 1:
                    if(revChar=='l') state = 2;
                    else state = 0;
                    break;
                case 2:
                    if(revChar=='s') state = 3;
                    else state = 0;
                    break;
                case 3:
                    if(revChar==0x0D)
                    {
                        Dsp_Clear();
                        LCDAddr = 0;
                        LcdPtr = 0;
                        revChar = 0x00;
                        state = 0;
                        Buzzer_On(2);
                    }
                    else state = 0;
                    break;
                
                case 4:
                    if(revChar=='w') state = 5;
                    else if(revChar=='r') state = 6; 
                    else state = 0;
                    break;
                
                case 5:
                    if(revChar==0x0D)
                    {
                        Eeprom_bwrite(0x00000010,(INT8U*)&eepTest);
                        revChar = eepTest;
                         state = 0;
                         Buzzer_On(2);
                    }
                    else state = 0;
                    break;
                case 6:
                    if(revChar==0x0D)
                    {
                        eepData = Eeprom_bread(0x00000010);
                        if (eepData == eepTest)
                        {
                            revChar = eepData;
                            Buzzer_On(2);
                        }
                        state = 0;
                    }
                    else state = 0;
                    break;
                 
                case 7:
                    if(revChar==0x0D)
                    {
                        Buzzer_On(1);
                        state = 0;
                    }
                    else state = 0;
                    break;
            }
            PutCharTxBuf(&CommBufUart1,revChar);
        }
        
        
        if(rawAdData) //kevin : 컨버젼된 AD 값이 있으면 rawAdData에 값이 생기고, 결국 여기로 진입하게된다.
        {
            dispRawWad = rawAdData;
            Disp_AD_RAW_DATA(dispRawWad); //kevin : 결과를 디스플레이에 뿌려주는 쪽
            //kevin! : 여기다가 UART 신호 뿌리는 것을 넣어줘야한다.
        }

/*
        //kevin : 32비트 2진수를 10진수로 변환한다음 배열에 넣기 //
        //if(!function_exists(numToChar))
        if(rawAdData)
        {
            
            int decConv=0;
            int adData_bin[32]=0;
            int adData_dec[10]=0;
            int i;
            
            adData_bin[32] = rawAdData;
            
            for(i=0;i<=32;i++) //kevin : 2진수를 10진수로
            {
                decConv *= 2;
                decConv += adData_bin[i] - '0'; //kevin : decConv에 adData_bin배열의 i번째 문자열의 코드값에서 '0'문자의 코드값을 뺀 값을 decConv 에 더한다.
            }
            //adData_dec[] = decConv; //kevin : adData_dec[0] (0비트)에 가장 큰 자릿수가 들어간다.
        }
*/
        
        if(rawTempAdData)
        {
            dispRawTad = rawTempAdData;
            Disp_Temp_AD_RAW_DATA(dispRawTad);
        }

        /*
        if(TimerCheckDelay100ms(TIMER_DELAY_100MS_SEND_DATA)) //kevin : 100ms 마다 데이터 뿌려주기
        {
            SendAdRawDATA(dispRawWad); //kevin : adData 를 UART 로 전송
            //SendAdRawNFLDATA(dispRawWad,dispRawTad); //kevin : 계량부와 온도 AD 값을 UART0 에 한꺼번에 뿌려주고 있음
            TimerSetDelay100ms(TIMER_DELAY_100MS_SEND_DATA,1);
        }
        */
        

        network_tx_proc();
    }
}

void RCC_Init(void)
{
  unsigned int PLLM = 8;
  unsigned int PLLN = 336;
  unsigned int PLLP = 4;
  unsigned int PLLQ = 7;
  
  RCC_DeInit();

  /* Enable HSE */
  RCC_HSEConfig(RCC_HSE_ON);
  
//  /* Wait till HSE is ready */
  while(RCC_GetFlagStatus(RCC_FLAG_HSERDY) == RESET);
  
  /* Flash 2 wait state */
  FLASH_SetLatency(FLASH_Latency_2);
  
  /* HCLK = SYSCLK */
  RCC_HCLKConfig(RCC_SYSCLK_Div1); 

  /* configure PLLCLK */
  RCC_PLLConfig(RCC_PLLSource_HSE, PLLM ,PLLN, PLLP, PLLQ);

  /* Enable PLL */ 
  RCC_PLLCmd(ENABLE);

  /* Wait till PLL is ready */
  while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET)
  {
  }

  /* Select PLL as system clock source */
  RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

  /* Wait till PLL is used as system clock source */
  while(RCC_GetSYSCLKSource() != 0x08)
  {
  }  
 
 
      
  /* configure PCLK2 */
  RCC_PCLK2Config(RCC_HCLK_Div1); 

  /* configure PCLK1 */
  RCC_PCLK1Config(RCC_HCLK_Div2);

    /* Enable Prefetch Buffer */
  FLASH_PrefetchBufferCmd(ENABLE);
  
  
    /* Enable Clock Security System(CSS) */
  RCC_ClockSecuritySystemCmd(ENABLE);
  
    
    
    
}

void OutString(char *s)
{
  while(*s)
  {
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET); // Wait for Empty
 
    USART_SendData(USART1, *s++); // Send Char
  }
}
/**
  * @brief  Inserts a delay time.
  * @param  nTime: specifies the delay time length, in milliseconds.
  * @retval None
  */
void Delay(__IO uint32_t nTime)
{ 
  uwTimingDelay = nTime;

  while(uwTimingDelay != 0);
}

/**
  * @brief  Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */
void TimingDelay_Decrement(void)
{
  if (uwTimingDelay != 0x00)
  { 
    uwTimingDelay--;
  }
}
void rcc_clock_init(void)
{
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3, ENABLE);
       /* HSE clock selected to output on MCO1 pin(PA8)*/
//  RCC_MCO1Config(RCC_MCO1Source_HSE, RCC_MCO1Div_1);
}

void gpio_init(void)
{
    
    GPIO_InitTypeDef GPIO_InitStructure;
    
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9|GPIO_Pin_10;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
    
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_SPI2);  //DISP_SPI_SCK
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_SPI2);   //DISP_SPI_MISO
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_SPI2);  //DISP_SPI_MOSI
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;  
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12; //DISP_SPI_NSS
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;  
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    
//    GPIO_PinAFConfig(GPIOB, GPIO_PinSource12, GPIO_AF_SPI2);  //DISP_SPI_NSS
 

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10|GPIO_Pin_11|GPIO_Pin_12;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13; //EEPROM CS
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;  
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_SPI3);   //EEPROM_SPI_SCK
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource11, GPIO_AF_SPI3);   //EEPROM_SPI_MISO
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource12, GPIO_AF_SPI3);  //EEPROM_SPI_MOSI
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11; //BUZZER
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;  
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3; //KEY OUT 1~4
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;  
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4; //KEY IN 4
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;  
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1; //KEY ads dout (in)
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0; //ads  sclk  (out)
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
 
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5; //ads pdwn (out)
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;  
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //Status LED 3
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;  
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2|GPIO_Pin_3 ; //Status LED 1, Status LED 2
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;  
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    GPIO_SetBits(GPIOA,GPIO_Pin_2);
    GPIO_ResetBits(GPIOA,GPIO_Pin_3);
    GPIO_ResetBits(GPIOC,GPIO_Pin_9);
    
    GPIO_SetBits(GPIOC,GPIO_Pin_13);
    GPIO_SetBits(GPIOB,GPIO_Pin_12);
    
#if defined(AD_DRV_AD7714)
                                   //SCLK    //DIN
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5|GPIO_Pin_7; //AD7714
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;  
    GPIO_Init(GPIOA, &GPIO_InitStructure);
     GPIO_SetBits(GPIOA,GPIO_Pin_5);
 
                                    //DOUT    //DRDY
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6|GPIO_Pin_4; //AD7714
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
#elif defined(AD_DRV_AD7177)
    
                                   //SYNC      //SCLK     //DIN
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_7; //AD7714
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;  
    GPIO_Init(GPIOA, &GPIO_InitStructure);
     GPIO_SetBits(GPIOA,GPIO_Pin_5);
 
                                    // DRDY / DOUT
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6; //AD7714
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
#endif
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0; //LTC2420 SCK
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;  
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    GPIO_ResetBits(GPIOA,GPIO_Pin_0);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1; //LTC2420 DOUT
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;  
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    
    GPIO_ResetBits(GPIOA,GPIO_Pin_3);
   
}

void nvic_init(void)
{
    NVIC_InitTypeDef NVIC_InitStructure; 
    /* NVIC configuration */
    /* Configure the Priority Group to 2 bits */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
    
    /* Enable the USARTx Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
    
    /* Enable the TIM2 global Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;  
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

/* Enable and configure RCC global IRQ channel, will be used to manage HSE ready 
     and PLL ready interrupts. 
     These interrupts are enabled in stm32f4xx_it.c file **********************/    
    NVIC_InitStructure.NVIC_IRQChannel = RCC_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;  
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

void usart1_init(void)
{
    USART_InitTypeDef USART_InitStructure;
    
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    
    USART_Init(USART1,&USART_InitStructure);
    
    /* Enable USART */
    USART_Cmd(USART1,ENABLE);
    
    USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
  
}

/**
  * @brief  Configures the SPI Peripheral.
  * @param  None
  * @retval None
  */
void SPI_Config(void)
{
    SPI_InitTypeDef SPI_InitStructure;
    /* SPI configuration -------------------------------------------------------*/
      
    
    //DISPLAY SPI INIT
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_16b;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(SPI2,&SPI_InitStructure);
    SPI_Cmd(SPI2, ENABLE);
    
    //EEPROM SPI INIT
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(SPI3,&SPI_InitStructure);
    SPI_Cmd(SPI3, ENABLE);
    

    
}
/**
  * @brief  Configures the RTC peripheral by selecting the clock source.
  * @param  None
  * @retval None
  */
static void RTC_Config(void)
{
  NVIC_InitTypeDef NVIC_InitStructure; 
  EXTI_InitTypeDef EXTI_InitStructure;

  /* Enable the PWR clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

  /* Allow access to RTC */
  PWR_BackupAccessCmd(ENABLE);

  /* LSI used as RTC source clock */
  /* The RTC Clock may varies due to LSI frequency dispersion. */   
  /* Enable the LSI OSC */ 
  RCC_LSICmd(ENABLE);

  /* Wait till LSI is ready */  
  while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
  {
  }

  /* Select the RTC Clock Source */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
   
  /* Enable the RTC Clock */
  RCC_RTCCLKCmd(ENABLE);

  /* Wait for RTC APB registers synchronisation */
  RTC_WaitForSynchro();

  /* Calendar Configuration with LSI supposed at 32KHz */
  RTC_InitStructure.RTC_AsynchPrediv = 0x7F;
  RTC_InitStructure.RTC_SynchPrediv  = 0xFF; /* (32KHz / 128) - 1 = 0xFF*/
  RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
  RTC_Init(&RTC_InitStructure);  

  /* EXTI configuration *******************************************************/
  EXTI_ClearITPendingBit(EXTI_Line22);
  EXTI_InitStructure.EXTI_Line = EXTI_Line22;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
  
  /* Enable the RTC Wakeup Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = RTC_WKUP_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);  

  /* Configure the RTC WakeUp Clock source: CK_SPRE (1Hz) */
  RTC_WakeUpClockConfig(RTC_WakeUpClock_CK_SPRE_16bits);
  RTC_SetWakeUpCounter(0x0);

  /* Enable the RTC Wakeup Interrupt */
  RTC_ITConfig(RTC_IT_WUT, ENABLE);
  
  /* Enable Wakeup Counter */
  RTC_WakeUpCmd(ENABLE);
}
void ringBuf_Init(void)
{
  
    
    
    
}
void timer2_init(void)
{
  TIM_TimeBaseInitTypeDef       TIM_TimeBaseStructure;
  
    /* TIM2 clock enable */   //max - 72MHz
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
  
  /* Time base configuration */
  TIM_TimeBaseStructure.TIM_Prescaler = 84;  //72Mhz/72=1Mhz
//  TIM_TimeBaseStructure.TIM_Period = 100;    //1Mhz/100=->100us timer      
//  TIM_TimeBaseStructure.TIM_Period = 500;    //1Mhz/100=->500us timer    
  TIM_TimeBaseStructure.TIM_Period = 1000;    //1Mhz/1000=->1ms timer      
  

  TIM_TimeBaseStructure.TIM_ClockDivision = 0x0;    
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; 
  
  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
 
  /* TIM enable counter */
  TIM_Cmd(TIM2, ENABLE);

  /* TIM IT enable */
  TIM_ITConfig(TIM2,TIM_IT_Update, ENABLE);


    
}

/**
  * @brief  Configures TIM5 to measure the LSI oscillator frequency. 
  * @param  None
  * @retval LSI Frequency
  */
static uint32_t GetLSIFrequency(void)
{
  NVIC_InitTypeDef   NVIC_InitStructure;
  TIM_ICInitTypeDef  TIM_ICInitStructure;
  RCC_ClocksTypeDef  RCC_ClockFreq;

  /* Enable the LSI oscillator ************************************************/
  RCC_LSICmd(ENABLE);
  
  /* Wait till LSI is ready */
  while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
  {}

  /* TIM5 configuration *******************************************************/ 
  /* Enable TIM5 clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);
  
  /* Connect internally the TIM5_CH4 Input Capture to the LSI clock output */
  TIM_RemapConfig(TIM5, TIM5_LSI);

  /* Configure TIM5 presclaer */
  TIM_PrescalerConfig(TIM5, 0, TIM_PSCReloadMode_Immediate);
  
  /* TIM5 configuration: Input Capture mode ---------------------
     The LSI oscillator is connected to TIM5 CH4
     The Rising edge is used as active edge,
     The TIM5 CCR4 is used to compute the frequency value 
  ------------------------------------------------------------ */
  TIM_ICInitStructure.TIM_Channel = TIM_Channel_4;
  TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
  TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
  TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV8;
  TIM_ICInitStructure.TIM_ICFilter = 0;
  TIM_ICInit(TIM5, &TIM_ICInitStructure);
  
  /* Enable TIM5 Interrupt channel */
  NVIC_InitStructure.NVIC_IRQChannel = TIM5_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

  /* Enable TIM5 counter */
  TIM_Cmd(TIM5, ENABLE);

  /* Reset the flags */
  TIM5->SR = 0;
    
  /* Enable the CC4 Interrupt Request */  
  TIM_ITConfig(TIM5, TIM_IT_CC4, ENABLE);

  /* Wait until the TIM5 get 2 LSI edges (refer to TIM5_IRQHandler() in 
    stm32f4xx_it.c file) ******************************************************/
  while(uwCaptureNumber != 2)
  {
  }
  /* Deinitialize the TIM5 peripheral registers to their default reset values */
  TIM_DeInit(TIM5);

  /* Compute the LSI frequency, depending on TIM5 input clock frequency (PCLK1)*/
  /* Get SYSCLK, HCLK and PCLKx frequency */
  RCC_GetClocksFreq(&RCC_ClockFreq);

  /* Get PCLK1 prescaler */
  if ((RCC->CFGR & RCC_CFGR_PPRE1) == 0)
  { 
    /* PCLK1 prescaler equal to 1 => TIMCLK = PCLK1 */
    return ((RCC_ClockFreq.PCLK1_Frequency / uwPeriodValue) * 8);
  }
  else
  { /* PCLK1 prescaler different from 1 => TIMCLK = 2 * PCLK1 */
    return (((2 * RCC_ClockFreq.PCLK1_Frequency) / uwPeriodValue) * 8) ;
  }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
